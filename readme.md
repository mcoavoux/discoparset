
# Discoparset

Discontinuous constituency parser.


If you want to reproduce experiments in the NAACL 2019 paper or use pretrained models, checkout the following release: https://gitlab.com/mcoavoux/discoparset/-/releases/v1.0

Discontinuous Constituency Parsing with a Stack-free Transition System and a Dynamic Oracle  
Maximin Coavoux, Shay B. Cohen  
NAACL 2019.  
[[pdf]](https://www.aclweb.org/anthology/N19-1018.pdf) [[bib]](https://www.aclweb.org/anthology/N19-1018.bib) [[abs/preprint]](https://arxiv.org/abs/1904.00615) [[hal]](https://hal.archives-ouvertes.fr/hal-02150076)


TODO: update for TALN 2020

## Install dependencies

Assuming you have a conda distribution of Python:

    git clone https://gitlab.com/mcoavoux/discoparset/
    cd discoparset
    conda create --name discoparset python=3.6 --file conda-requirements.txt
    conda activate discoparset
    pip install -r requirements.txt

Install [disco-dop](https://github.com/andreasvc/disco-dop/).

## Train parser

    python main.py train <modelname>  ...train.discbracket ....dev.discbracket  --fmt=discbracket  [--bert concat --bert-id bert-base-cased] --gpu 0   [ other options ]

## Parse your own text

The input file should contain one sentence per line, with the same tokenization conventions as in the PTB.

    python main.py eval <path to pretrained model> input_file output_file [--gpu <gpu_id>]


## Reproduce results in paper with pretrained models:


Generate datasets:

    git clone https://github.com/mcoavoux/multilingual_disco_data.git
    # and follow instructions in multilingual_disco_data/readme

Parse:

    cd src
    # python main.py eval <pretrained model>  <input: 1 tokenized sentence per line> <output> [--gpu <gpu_id>] [--gold gold.discbracket]
    python main.py eval ../pretrained_models/dptb_bert_replace/  ../multilingual_disco_data/data/dptb/dev.tokens ptb_dev.out --gpu 0 --gold ../multilingual_disco_data/data/dptb/dev.discbracket

    # --gpu 0  -> use gpu number 0
    # --gold gold.discbracket: evaluate output against gold corpus after parsing

    # Expected results
    ...
    precision=94.81
    recall=94.82
    fscore=94.82
    disc-precision=80.05
    disc-recall=77.35
    disc-fscore=78.68



