import sys
import os
from collections import defaultdict
import copy
import logging
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import time
import random

from bert import BertEncoder
from Asgd import MyAsgd
import character_encoders as enc
import discodop_eval
from state import State
import corpus_reader
import tree as T

from transformer import TransformerEncoderLayer


class Structure(nn.Module):
    """Structure classifier batches """
    def __init__(self, input_size, dropout, hidden, atn):
        super(Structure, self).__init__()
        self.input_size = input_size
        input_dim = input_size
        if atn:
            input_dim = (input_size // 2) * 3
        self.structure = nn.Sequential(
                            nn.Dropout(dropout),
                            nn.Linear(input_dim, hidden),
                            nn.Tanh(),
                            nn.Linear(hidden, hidden),
                            nn.Tanh(),
                            nn.Linear(hidden, 1, bias=False))
        self.softmax = nn.LogSoftmax(dim=0)
        self.atn = atn

    def forward(self, input_list):
        if type(input_list) == list:
            lengths = [len(input) for input in input_list]
            padded = torch.cat(input_list, dim=0)
            out_layer = self.structure(padded)
            split_out = out_layer.split(lengths)
            return [self.softmax(out).view(1, -1) for out in split_out]
        out_layer = self.structure(input_list)
        return self.softmax(out_layer).view(1, -1)
        

class Transducer(nn.Module):
    #memory_sizes = []
    """Stores sentence level bi-lstm and all other paramters of parser (struct and label classifiers)"""
    def __init__(self,
                 args, 
                 voc_size,
                 word_voc_size,
                 num_labels,
                 num_tag_labels,
                 words2tensors,
                 bert,
                 bert_id):
        super(Transducer, self).__init__()

        # Using word embeddings is optional
        self.word_embeddings = None
        transducer_input_size = args.C
        if args.w is not None:
            self.word_embeddings = nn.Embedding(word_voc_size, args.w, padding_idx=0)
            transducer_input_size += args.w

        self.bert_mode = bert
        self.bert = None
        dim_encoder = args.W
        if bert in {"replace", "concat"}:
            self.bert = BertEncoder(bert_id)
            if bert == "replace":
                dim_encoder = self.bert.dim
            if bert == "concat":
                dim_encoder = self.bert.dim + args.W

        self.char_encoder = enc.CharacterLstmLayer(
                                emb_dim=args.c,
                                voc_size=voc_size,
                                out_dim=args.C,
                                embed_init=args.I,
                                words2tensors=words2tensors,
                                dropout=args.K)

        self.word_transducer_l1 = nn.LSTM(input_size=transducer_input_size,
                                       hidden_size=args.W //2,
                                       num_layers=1,
                                       batch_first=True,
                                       bidirectional=True)

        assert(args.P >= 2)
        self.word_transducer_l2 = nn.LSTM(input_size=args.W,
                                       hidden_size=args.W //2,
                                       num_layers=args.P -1,
                                       batch_first=True,
                                       bidirectional=True)

        self.dropout = nn.Dropout(args.Q)

        # Structural actions
        self.structure = Structure(dim_encoder*8, args.D, args.H, args.A)

        # Labelling actions
        self.label = nn.Sequential(
                        nn.Dropout(args.D),
                        nn.Linear(dim_encoder*4, args.H),
                        nn.Tanh(),
                        nn.Linear(args.H, args.H),
                        nn.Tanh(),
                        nn.Linear(args.H, num_labels, bias=False),
                        nn.LogSoftmax(dim=1))

        # POS tags
        self.tagger = nn.Sequential(
                        nn.Dropout(args.X),
                        nn.Linear(dim_encoder, num_tag_labels, bias=False),
                        nn.LogSoftmax(dim=1))
        self.tagger_loss = nn.NLLLoss(reduction="sum")

        # "Padding" parameters for constituents with no gaps
        self.default_values = nn.Parameter(torch.Tensor(4, dim_encoder))
        self.default_values.data.uniform_(-0.01, 0.01)

        self.initialize_parameters()

    def initialize_parameters(self):
        # Xavier initialization for every layer
        # uniform initialization for embeddings
        if self.word_embeddings is not None:
            self.word_embeddings.weight.data.uniform_(-args.I, args.I)
        for mod in [self.word_transducer_l1, self.word_transducer_l2, self.structure, self.label, self.tagger]:
            for p in mod.parameters():
                n_ids = sum(p.data.shape)
                m = (6 / n_ids)**0.5
                p.data.uniform_(-m, m)

    def forward_sentence(self, sentence, l1_only=False, batch=False):
        if not batch:
            char_based_embeddings = self.char_encoder(sentence[0])

            char_based_embeddings = self.dropout(char_based_embeddings)
            if self.word_embeddings is not None:
                embeds = self.word_embeddings(sentence[1])
                char_based_embeddings = torch.cat([char_based_embeddings, embeds], dim=1)
            
            output_l1, (h_n, c_n) = self.word_transducer_l1(char_based_embeddings.unsqueeze(0))
            if l1_only:
                return output_l1.squeeze(0), None

            output_l2, (h_n, c_n) = self.word_transducer_l2(output_l1)
            output_l2 = output_l2 + output_l1 # residual connections
            #output_l2 = torch.cat([self.default_values, output_l2.squeeze(0)])
            return output_l1.squeeze(0), output_l2.squeeze(0)
        else:
            sentence, all_words = zip(*sentence)
            lengths = [len(s) for s in sentence]

            all_tokens = [tok for s in sentence for tok in s]
            char_based_embeddings = self.char_encoder(all_tokens)
            char_based_embeddings = self.dropout(char_based_embeddings)
            char_based_embeddings = char_based_embeddings.split(lengths)

            if self.word_embeddings is not None:
                embeds = [self.word_embeddings(words) for words in all_words]
                char_based_embeddings = [torch.cat([cb_es, w_e], dim=1)
                                         for cb_es, w_e in zip(char_based_embeddings, embeds)]

            padded_char_based_embeddings = torch.nn.utils.rnn.pad_sequence(char_based_embeddings, batch_first=True)
            packed_padded_char_based_embeddings = torch.nn.utils.rnn.pack_padded_sequence(
                                    padded_char_based_embeddings, lengths, batch_first=True)

            output_l1, (h_n, c_n) = self.word_transducer_l1(packed_padded_char_based_embeddings)
            output_l1, _ = torch.nn.utils.rnn.pad_packed_sequence(output_l1, batch_first=True)

            if l1_only:
                output_l1 = [t.squeeze(0) for t in output_l1.split([1 for l in lengths], dim=0)]
                output_l1 = [t[:l,:] for t, l in zip(output_l1, lengths)]
                return output_l1, None

            packed_l1 = torch.nn.utils.rnn.pack_padded_sequence(output_l1, lengths, batch_first=True)

            output_l2, (h_n, c_n) = self.word_transducer_l2(packed_l1)
            unpacked_l2, _ = torch.nn.utils.rnn.pad_packed_sequence(output_l2, batch_first=True)

            output_l2 = unpacked_l2 + output_l1 # residual connections

            output_l1 = [t.squeeze(0) for t in output_l1.split([1 for l in lengths], dim=0)]
            output_l1 = [t[:l,:] for t, l in zip(output_l1, lengths)]
            output_l2 = [t.squeeze(0) for t in output_l2.split([1 for l in lengths], dim=0)]
            output_l2 = [t[:l,:] for t, l in zip(output_l2, lengths)]

            #output_l2 = [torch.cat([self.default_values, ol2], dim=0) for ol2 in output_l2]
            return output_l1, output_l2


    def forward(self, sentence, l1_only=False, batch=False):
        """
        l1_only: only computes first layer of bilstm (for training the tagger)
        batch: true if sentence is a list of sentences"""
        if not batch:
            if self.bert_mode in {"replace", "concat"}:
                bert_embeddings = self.bert(sentence[0], batch=batch)
            if self.bert_mode == "replace":
                return bert_embeddings, torch.cat([self.default_values, bert_embeddings])

            output_l1, output_l2 = self.forward_sentence(sentence, l1_only, batch)
            if self.bert_mode is None:# or self.bert_mode.startswith("distill"):
                if l1_only:
                    return output_l1, None
                return output_l1, torch.cat([self.default_values, output_l2], dim=0)

            assert(self.bert_mode == "concat")
            l1 = torch.cat([bert_embeddings, output_l1], dim=1)
            if l1_only:
                return l1, None
            l2 = torch.cat([bert_embeddings, output_l2], dim=1)
            l2 = torch.cat([self.default_values, l2], dim=0)
            return l1, l2 

        else:
            if self.bert_mode in {"replace", "concat"}:
                bert_embeddings = self.bert([s[0] for s in sentence], batch=batch)
            if self.bert_mode == "replace":
                return bert_embeddings, [torch.cat([self.default_values, bert_e], dim=0) for bert_e in bert_embeddings]

            output_l1, output_l2 = self.forward_sentence(sentence, l1_only, batch)
            if self.bert_mode is None: # or self.bert_mode.startswith("distill"):
                if l1_only:
                    return output_l1, None
                return output_l1, [torch.cat([self.default_values, ol2], dim=0) for ol2 in output_l2]

            assert(self.bert_mode == "concat")
            l1 = [torch.cat([be, ol1], dim=1) for be, ol1 in zip(bert_embeddings, output_l1)]
            if l1_only:
                return l1, None
            l2 = [torch.cat([be, ol2], dim=1) for be, ol2 in zip(bert_embeddings, output_l2)]
            l2 = [torch.cat([self.default_values, ol2], dim=0) for ol2 in l2]
            return l1, l2

def get_vocabulary(corpus):
    """Extract vocabulary for characters, tokens, non-terminals, POS tags"""
    words = defaultdict(int)

    chars = defaultdict(int)
    chars["<START>"] += 2
    chars["<STOP>"] += 2

    # These should be treated as single characters
    chars["-LRB-"] += 2
    chars["-RRB-"] += 2
    chars["#LRB#"] += 2
    chars["#RRB#"] += 2

    tag_set = defaultdict(int)

    label_set = defaultdict(int)
    for tree in corpus:
        tokens = T.get_yield(tree)
        for tok in tokens:
            for char in tok.token:
                chars[char] += 1

            tag_set[tok.get_tag()] += 1

            words[tok.token] += 1

        constituents = T.get_constituents(tree)
        for label, _ in constituents:
            label_set[label] += 1

    return words, chars, label_set, tag_set


def sentence_to_tensors(sentence, char2i, words2i, device):
    # mapping from word to chars (including special cases treatment such as -LRB- -RRB- are taken care of in character_encoders
    words = [words2i[w] if w in words2i else words2i["<UNK>"] for w in sentence]
    return (sentence, torch.tensor(words, dtype=torch.long, device=device))

def batch_stochastic_replacement(device, tensors, copy_tensors, words2i, pword=0.3):
    """
    Reinitializes and modify in place copy_tensors
    Args:
        device, chars2i, words2i: device object, mappers character/index to index
        tensors: original long tensors for tokens and chars of train corpus
        copy_tensors: slightly differ from original and used as input for training
        pchar: replaces each character by "<UNK>" with probability pchar
        pword: replaces each low frequency token by "<UNK>" with probability pword
                low frequency = 2/3 least frequent tokens
    """
    # indexes for unknown char and unknown word
    #cunk = chars2i["<UNK>"]
    wunk = words2i["<UNK>"]
    # only replace the 2/3 less frequent words
    repi = len(words2i) // 3
    
    #longest = max([len(w) for w in words2i]) + 3
    #cmaskr = torch.rand(longest, device=device)
    # uint8: boolean selection of indices (torch.long -> index selection)
    #cmaski = torch.tensor(list(range(longest)), dtype=torch.uint8, device=device) 

    # 150: hard constraint on length of sentence
    wmaskr = torch.rand(150, device=device)
    wmaski = torch.tensor(list(range(150)), dtype=torch.bool, device=device)

    for s1, s2 in zip(tensors, copy_tensors):
#        for c1, c2 in zip(s1[0], s2[0]):
#            c2.copy_(c1)
#            cmaskr.uniform_(0, 1)
#            cmaski.copy_(cmaskr > (1-pchar))
#            cmaski[0] = 0 # do not replace <START> and <STOP> symbols
#            cmaski[-1] = 0
#            c2[cmaski[:len(c2)]] = cunk

        w1, w2 = s1[1], s2[1]
        w2.copy_(w1)
        wmaskr.uniform_(0, 1)
        wmaski.copy_(wmaskr > (1-pword))

        wmaski[:len(w2)] *= w2 > repi
        w2[wmaski[:len(w2)]] = wunk

def set2tensor(device, iset, add=None, embeddings=None):
    # Returns a representation of a constituent, i.e. set of indexes as either
    # - a tensor of token indexes
    # - a tensor of token context-aware embeddings
    shift = 4     # shift bc first indexes of embedding matrix are reserved for additional parameters
    mini = min(iset)
    maxi = max(iset)
    gapi = {i for i in range(mini, maxi+1) if i not in iset}
    mingapi = 2
    maxgapi = 3
    if len(gapi) > 0:
        mingapi = min(gapi) + shift
        maxgapi = max(gapi) + shift
    
    iset = [mini+shift, maxi+shift, mingapi, maxgapi]

    # Deactivated for now
    if False and add is not None:
        iset.append(add)
    input_set_tensors = torch.tensor(iset, dtype=torch.long, device=device)
    if embeddings is None:
        return input_set_tensors
    return embeddings[input_set_tensors].view(-1)

def embed_and_parse_one(device, model, i2labels, i2tags, sentence, sentence_tensors):
    # Parse single sentence
    with torch.no_grad():
        state = State(sentence)
        embeddings_l1, embeddings_l2 = model(sentence_tensors)
        return parse(device, model, i2labels, i2tags, sentence, state, embeddings_l1, embeddings_l2)

def embed_and_parse_batch(device, model, i2labels, i2tags, sentences, sentences_tensors):
    # Parse batch of sentences
    with torch.no_grad():
        trees = []
        embeddings_l1, embeddings_l2 = model(sentences_tensors, batch=True)
        for el1, el2, sent in zip(embeddings_l1, embeddings_l2, sentences):
            state = State(sent)
            trees.append(parse(device, model, i2labels, i2tags, sent, state, el1, el2))
        return trees

def embed_and_extract_dyn_oracle(device, model, labels2i, i2labels, sentences, sentences_tensors, gold_constituents):
    with torch.no_grad():
        oracles = []
        _, embeddings_l2 = model(sentences_tensors, batch=True)
        for el2, sent, gconst in zip(embeddings_l2, sentences, gold_constituents):
            state = State(sent)
            oracles.append(extract_dyn_oracle(device, model, labels2i, i2labels, sent, state, el2, gconst))
        return oracles

def extract_dyn_oracle_from_corpus(device, model, labels2i, i2labels, sentences_copy, tensors, gold_constituents, p):
    # keep training example with probability p
    ps = np.random.rand(len(gold_constituents)) > (1-p)
    idxes = np.arange(len(gold_constituents))
    idxes = idxes[ps]

    sentences = [sentences_copy[i] for i in idxes]
    tensors = [tensors[i] for i in idxes]
    gold_constituents = [gold_constituents[i] for i in idxes]

    idxes, sentences, tensors, gold_constituents = zip(*sorted(zip(
                                idxes,
                                sentences, 
                                tensors,
                                gold_constituents), 
                               key = lambda x: len(x[1]),
                               reverse=True))

    gold_constituents = [{tuple_idx:label for label, tuple_idx in gc} for gc in gold_constituents]

    oracles = []
    batch_size=100
    for i in range(0, len(sentences), batch_size):
        oracles.extend(embed_and_extract_dyn_oracle(device,
                                                     model, 
                                                     labels2i,
                                                     i2labels,
                                                     sentences[i:i+batch_size],
                                                     tensors[i:i+batch_size],
                                                     gold_constituents[i:i+batch_size]))
    assert(len(idxes) == len(oracles))
    return list(zip(idxes, oracles))


def extract_dyn_oracle(device, model, labels2i, i2labels, sentence, state, embeddings_l2, gold_constituents):

    oinput_struct  = []
    ooutput_struct = []
    oinput_labels  = []
    ooutput_labels = []

    #return embeddings[input_set_tensors].view(-1)

    while not state.is_final():
        next_type = state.next_action_type()
        if next_type == State.STRUCT:
            memory, focus, buf = state.get_structural_step_input()
            if focus is None or len(memory) == 0:
                state.shift()
                continue

            # append input
            focus_tensor = set2tensor(device, focus)
            memory_tensor = [torch.cat([set2tensor(device,
                                                input_set,
                                                add=0),
                                        focus_tensor])
                             for input_set in memory]
            if buf is not None:
                memory_tensor.append(torch.cat([set2tensor(device, buf, add=1), focus_tensor]))

            #mem = torch.stack(memory_tensor)
            #foc = focus_tensor
            struct_input = torch.stack(memory_tensor)
            oinput_struct.append(struct_input)

            em_input = embeddings_l2[struct_input].view(len(memory_tensor), -1)
            log_probs = model.structure(em_input).view(-1)

            
            best_action = state.dyn_oracle(gold_constituents)

            if best_action[0] == "combine":
                target = best_action[1]
            else:
                target = len(memory_tensor) - 1
            ooutput_struct.append(target)

            probs = torch.exp(log_probs)
            action_id = np.random.choice(len(memory_tensor), p=probs.cpu().numpy())
            if action_id == len(memory):
                state.shift()
            else:
                state.combine(action_id)
        else:
            assert(next_type == State.LABEL)
            input_set = state.get_labelling_step_input()
            #input_tensor = set2tensor(device, input_set, embeddings=embeddings_l2)
            input_tensor = set2tensor(device, input_set)

            oinput_labels.append(input_tensor)
            best_action = state.dyn_oracle(gold_constituents)
            ooutput_labels.append(labels2i[best_action[1]])

            em_input = embeddings_l2[input_tensor].view(1, -1)

            log_probs = model.label(em_input).view(-1)

            if state.is_prefinal(): # Forbid no-label for the constituent that spans the whole sentence (need a root label)
                prediction = torch.argmax(log_probs[1:]) + 1
            else:
                prediction = torch.argmax(log_probs)

            # For dynamic oracle training: the labelling action does not matter
            # (bc no error propagation from labels)
            if prediction == 0:
                state.nolabel()
            else:
                prediction_str = i2labels[prediction]
                state.labelX(prediction_str)

    r1, r2 = None, None
    if len(oinput_struct) > 0:
        #r1 = torch.stack(struct_input)
        #r2 = torch.cat(struct_output)
        r1 = oinput_struct
        r2 = torch.tensor(ooutput_struct, dtype=torch.long, device=device).view(-1, 1)
    r3 = torch.stack(oinput_labels)
    r4 = torch.tensor(ooutput_labels, dtype=torch.long, device=device)

    return r1, r2, r3, r4


def parse(device, model, i2labels, i2tags, sentence, state, embeddings_l1, embeddings_l2):
    # Predict POS tags and parse, returns a tree
    # Embeddings_l1, embeddings_l2: output of sentence level bi-LSTM

    # predict and assign pos tags
    tag_scores = model.tagger(embeddings_l1)
    tag_predictions = torch.argmax(tag_scores, dim=1).cpu().numpy()
    for tag, tok in zip(tag_predictions, sentence):
        tok.set_tag(i2tags[tag])

    # Parsing
    while not state.is_final():
        next_type = state.next_action_type()
        if next_type == State.STRUCT:
            memory, focus, buf = state.get_structural_step_input()
            #model.memory_sizes.append(len(memory))
            if focus is None or len(memory) == 0:
                state.shift()
                continue

            focus_tensor = set2tensor(device, focus, embeddings=embeddings_l2)
            memory_tensor = [set2tensor(device,
                                        input_set,
                                        add=0, 
                                        embeddings=embeddings_l2)
                             for input_set in memory]
            if buf is not None:
                memory_tensor.append(set2tensor(device, buf, add=1, embeddings=embeddings_l2))
            mem = torch.stack(memory_tensor)
            foc = focus_tensor
            struct_input = torch.cat([mem, foc.repeat(len(memory_tensor), 1)], dim=1)

            log_probs = model.structure(struct_input).view(-1)
            action_id = torch.argmax(log_probs)
            if action_id == len(memory):
                state.shift()
            else:
                state.combine(action_id)
        else:
            assert(next_type == State.LABEL)
            input_set = state.get_labelling_step_input()
            input_tensor = set2tensor(device, input_set, embeddings=embeddings_l2)

            log_probs = model.label(input_tensor.unsqueeze(0)).view(-1)

            if state.is_prefinal(): # Forbid no-label for the constituent that spans the whole sentence
                prediction = torch.argmax(log_probs[1:]) + 1
            else:
                prediction = torch.argmax(log_probs)

            if prediction == 0:
                state.nolabel()
            else:
                prediction_str = i2labels[prediction]
                state.labelX(prediction_str)
    return state.get_tree()


def train_sentence_batch(device, model, optimizer, sentence_tensors, features, batch=False, dynamic=None): #, bert_encoding=None):
    # Computes the parsing loss for a sentence
    # in batch mode (batch all structure and label predictions)
    # args:
    #   sentence_tensors: representations of chars and tokens in sentence
    #   features: input representation for each action
    #   batch: True if the input consists of several sentences
    # returns:
    #   loss: the negative log likelihood loss node
    optimizer.zero_grad()
    if not batch:
        struct_input, struct_output, labels_input, labels_output = features
        if dynamic is not None:
            struct_input, struct_output, labels_input, labels_output = dynamic
#            ds_input, ds_output, dl_input, dl_output = dynamic
#            if ds_input is not None:
#                struct_input = struct_input + ds_input
#                struct_output = torch.cat([struct_output, ds_output])
#            labels_input = torch.cat([labels_input, dl_input])
#            labels_output = torch.cat([labels_output, dl_output])

        input_tensors = sentence_tensors
        _, embeddings = model(input_tensors)

        #labels_input_tensors = embeddings[labels_input].view(len(labels_input), -1)
        labels_input_tensors = F.embedding(labels_input, embeddings).view(len(labels_input), -1)

        labels_output_tensors = model.label(labels_input_tensors)
        loss = F.nll_loss(labels_output_tensors, labels_output, reduction="sum")

        if struct_input is not None:
            # ++ 
            #struct_input_tensors = [embeddings[str_in].view(len(str_in), -1) for str_in in struct_input]
            struct_input_tensors = [F.embedding(str_in, embeddings).view(len(str_in), -1) for str_in in struct_input]
            # ++ 
            
            struct_output_tensors = model.structure(struct_input_tensors)
            for i, so in enumerate(struct_output_tensors):
                loss += F.nll_loss(so, struct_output[i], reduction="sum")
        # --

        loss.backward()
        return loss.float()
    else:
        #assert bert_encoding is None, "Minibatch training not supported with bert"
        input_tensors = sentence_tensors

        _, embeddings = model(input_tensors, batch=True)

        # construct batch input / output for each sentence
        batch_label_input = []
        batch_label_output = []
        batch_struct_input = []
        batch_struct_output = []
        for i, (e, feat) in enumerate(zip(embeddings, features)): # iterates on output of bi-lstm for each sentence
            if dynamic is None:
                struct_input, struct_output, labels_input, labels_output = feat
                batch_label_input.append(e[labels_input].view(len(labels_input), -1))
                batch_label_output.append(labels_output)
                if struct_input is not None:
                    batch_struct_input.extend([e[str_in].view(len(str_in), -1) for str_in in struct_input])
                    batch_struct_output.append(struct_output)
            #if dynamic is not None:
            else:
                ds_input, ds_output, dl_input, dl_output = dynamic
                batch_label_input.append(e[dl_input].view(len(dl_input), -1))
                batch_label_output.append(dl_output)
                if ds_input is not None:
                    batch_struct_input.extend([e[str_in].view(len(str_in), -1) for str_in in ds_input])
                    batch_struct_output.append(ds_output)

        batch_label_input = torch.cat(batch_label_input, dim=0)
        batch_label_output = torch.cat(batch_label_output)

        labels_output_tensors = model.label(batch_label_input)
        loss = F.nll_loss(labels_output_tensors, batch_label_output, reduction="sum")

        if len(batch_struct_input) > 0:
            #batch_struct_output = torch.cat(batch_struct_output)
            struct_output_tensors = model.structure(batch_struct_input)
            struct_targets = torch.cat(batch_struct_output, dim=0)
            for i, so in enumerate(struct_output_tensors):
                loss += F.nll_loss(so, struct_targets[i], reduction="sum")

        loss /= len(features)
        loss.backward()
        return loss.float()




def train_sentence_tagging(device, model, optimizer, sentence_tensors, tags, batch=False):
    # Returns loss node for tagging
    optimizer.zero_grad()
    if not batch:
        input_tensors = sentence_tensors
        embeddings, _ = model(input_tensors, l1_only=True)
        output = model.tagger(embeddings)
        loss = torch.sum(model.tagger_loss(output, tags))
        loss.backward()
        return loss.float()
    else:
        input_tensors = sentence_tensors
        embeddings, _ = model(input_tensors, l1_only=True, batch=True)
        
        batch_input = torch.cat(embeddings, dim=0)
        batch_tags = torch.cat(tags)
        output = model.tagger(batch_input)
        loss = torch.sum(model.tagger_loss(output, batch_tags))
        loss /= len(tags)
        loss.backward()
        return loss.float()

def extract_features(device, labels2i, sentence):

    state = State(sentence)

    struct_input = []
    struct_output = []

    labels_input = []
    labels_output = []

    #labels_input_sets = set()

    while not state.is_final():
        next_type = state.next_action_type()
        if next_type == State.STRUCT:
            gold_action, (memory, focus, buf) = state.oracle()
        
            if focus is None or len(memory) == 0:
                continue

            focus_tensor = set2tensor(device, focus)
            input_tensors = [torch.cat([set2tensor(device, input_set, add=0), focus_tensor]) for input_set in memory]

            if buf is not None:
                input_tensors.append(torch.cat([set2tensor(device, buf, add=1), focus_tensor]))

            struct_input.append(torch.stack(input_tensors))

            if gold_action[0] == "combine":
                target = gold_action[1] 
            else:
                target = len(input_tensors) - 1

            struct_output.append(target)
        else:
            assert(next_type == State.LABEL)
            gold_action, input_set = state.oracle()
            input_tensor = set2tensor(device, input_set)
            labels_input.append(input_tensor)
            target = labels2i[gold_action[1]]
            labels_output.append(target)

            #labels_input_sets.add(tuple(input_set))

#    if len(struct_input) > 0:
#        struct_input, struct_output = zip(*sorted(list(zip(struct_input, struct_output)), key=lambda x: len(x), reverse=True))

    r1, r2 = None, None
    if len(struct_input) > 0:
        #r1 = torch.stack(struct_input)
        #r2 = torch.cat(struct_output)
        r1 = struct_input
        r2 = torch.tensor(struct_output, dtype=torch.long, device=device).view(-1, 1)
    r3 = torch.stack(labels_input)
    r4 = torch.tensor(labels_output, dtype=torch.long, device=device)

    return r1, r2, r3, r4


def extract_tags(device, tags2i, sentence):
    # Returns a tensor for tag ids for a single sentence
    idxes = [tags2i[tok.get_tag()] for tok in sentence]
    return torch.tensor(idxes, dtype=torch.long, device=device)

def compute_f(TPs, total_golds, total_preds):
    p, r, f = 0, 0, 0
    if total_preds > 0:
        p = TPs / total_preds
    if total_golds > 0:
        r = TPs / total_golds
    if (p, r) != (0, 0):
        f = 2*p*r / (p+r)
    return p, r, f

def Fscore_corpus(golds, preds):
    TPs = 0
    total_preds = 0
    total_golds = 0
    UTPs = 0
    for gold, pred in zip(golds, preds):
        TPs += len([c for c in gold if c in pred])
        total_golds += len(gold)
        total_preds += len(pred)

        ugold = defaultdict(int)
        for _, span in gold:
            ugold[span] += 1
        upred = defaultdict(int)
        for _, span in pred:
            upred[span] += 1
        for span in upred:
            UTPs += min(upred[span], ugold[span])

    p, r, f = compute_f(TPs, total_golds, total_preds)

    up, ur, uf = compute_f(UTPs, total_golds, total_preds)
    return p*100, r*100, f*100, up*100, ur*100, uf*100

def predict_corpus(device, model, i2labels, i2tags, sentences_copy, tensors, batch=True):

    trees = []
    if not batch:
        for toks, tens in zip(sentences_copy, tensors):
            tree = embed_and_parse_one(device, model, i2labels, i2tags, toks, tens)
            tree.expand_unaries()
            trees.append(tree)
        return trees
    else:
        # sort by length for lstm batching
        indices, sentences_copy, tensors = zip(*sorted(zip(range(len(sentences_copy)), 
                                                           sentences_copy,
                                                           tensors), 
                                                       key = lambda x: len(x[1]),
                                                       reverse=True))
        batch_size=200
        for i in range(0, len(sentences_copy), batch_size):
            tree_batch = embed_and_parse_batch(device, 
                                               model,
                                               i2labels, i2tags,
                                               sentences_copy[i:i+batch_size],
                                               tensors[i:i+batch_size])
            for t in tree_batch:
                t.expand_unaries()
                trees.append(t)
        # reorder
        _, trees = zip(*sorted(zip(indices, trees), key = lambda x:x[0]))
        return trees


def prepare_corpus(corpus, char2i, words2i, device):
    sentences = [T.get_yield(corpus[i]) for i in range(len(corpus))]
    raw_sentences = [[tok.token for tok in sentence] for sentence in sentences]
    sentences_copy = [[T.Token(tok.token, i, [feat for feat in tok.features])
                        for i, tok in enumerate(sent)] for sent in sentences]
    tensors = [sentence_to_tensors(sent, char2i, words2i, device) for sent in raw_sentences]
    return sentences, raw_sentences, sentences_copy, tensors


def eval_tagging(gold, pred):
    # Returns accuracy for tag predictions
    acc = 0
    tot = 0
    assert(len(gold) == len(pred))
    for sent_g, sent_p in zip(gold, pred):
        assert(len(sent_g) == len(sent_p))
        for tok_g, tok_p in zip(sent_g, sent_p):
            if tok_g.get_tag() == tok_p.get_tag():
                acc += 1
        tot += len(sent_g)
    return acc * 100 / tot


def save_dict(d, filename):
    # Saves dictionary to filename
    with open(filename, "w") as f:
        for k in d:
            f.write("{}\n".format(k))

def load_dict(filename):
    # Loads dictionary from filename
    i2l = []
    with open(filename, "r") as f:
        for k in f:
            i2l.append(k.strip())
    return i2l, {k: i for i, k in enumerate(i2l)}

def main_train(args, logger, device):

    logger.info("Loading corpora...")
    if args.fmt == "ctbk":
        train_corpus = corpus_reader.read_ctbk_corpus(args.train)
        dev_corpus = corpus_reader.read_ctbk_corpus(args.dev)
    elif args.fmt == "discbracket":
        train_corpus = corpus_reader.read_discbracket_corpus(args.train)
        dev_corpus = corpus_reader.read_discbracket_corpus(args.dev)

    for tree in train_corpus:
        tree.merge_unaries()

    logger.info("Vocabulary extraction...")
    words, vocabulary, label_set, tag_set = get_vocabulary(train_corpus)

    i2chars = ["<PAD>", "<UNK>"] + sorted(vocabulary, key = lambda x: vocabulary[x], reverse=True)
    chars2i = {k:i for i, k in enumerate(i2chars)}

    i2labels = ["nolabel"] + sorted(label_set)
    labels2i = {k: i for i, k in enumerate(i2labels)}

    i2tags = sorted(tag_set)
    tags2i = {k:i for i, k in enumerate(i2tags)}

    i2words = ["<PAD>", "<UNK>"] + sorted(words, key=lambda x: words[x], reverse=True)
    words2i = {k:i for i, k in enumerate(i2words)}

    save_dict(i2chars, "{}/i2chars".format(args.model))
    save_dict(i2labels, "{}/i2labels".format(args.model))
    save_dict(i2tags, "{}/i2tags".format(args.model))
    save_dict(i2words, "{}/i2words".format(args.model))

    random.shuffle(train_corpus)

    if args.S is not None:
        train_corpus = train_corpus[:args.S]
        dev_corpus = dev_corpus[:args.S]

    print("Training sentences: {}".format(len(train_corpus)))
    print("Dev set sentences: {}".format(len(dev_corpus)))

    logger.info("Model initialization...")

    words2tensors = enc.Words2Tensors(device, chars2i, words2i, pchar=None)
    model = Transducer(args, len(i2chars), len(i2words), len(labels2i), len(tags2i), words2tensors, args.bert, args.bert_id)
    model.to(device)

    num_parameters = 0
    num_parameters_embeddings = 0
    for name, p in model.named_parameters():
        print("parameter '{}', {}".format(name, p.numel()))
        if "embedding" in name:
            num_parameters_embeddings += p.numel()
        else:
            num_parameters += p.numel()
    print("Total number of parameters: {}".format(num_parameters + num_parameters_embeddings))
    print("Embedding parameters: {}".format(num_parameters_embeddings))
    print("Non embedding parameters: {}".format(num_parameters))

    # TODO: check customized learning rate for BERT
    if args.bert in {"replace", "concat"}:
        named_parameters = list(model.named_parameters())
        bert_params  = [p for name, p in named_parameters if name.startswith("bert")]
        other_params = [p for name, p in named_parameters if not name.startswith("bert")]
        assert(len(bert_params) + len(other_params) == len(named_parameters))
        parameters = [{"params": other_params}, {"params": bert_params, "lr": args.l / 10}]
    else:
        parameters = model.parameters()

    if args.O == "asgd":
        optimizer = MyAsgd(parameters, lr=args.l, 
                           momentum=args.m, weight_decay=0,
                           dc=args.d)
    elif args.O == "adam":
        optimizer = optim.Adam(parameters, lr=args.l)
    else:
        optimizer = optim.SGD(parameters, lr=args.l, momentum=args.m, weight_decay=0)


    logger.info("Constructing training examples...")
    corpus_tmp = prepare_corpus(train_corpus, chars2i, words2i, device)
    train_sentences, train_raw_sentences, train_sentences_copy, train_tensors = corpus_tmp
    #TODO all_gold_train_constituents = []

    dev_sentences, dev_raw_sentences, dev_sentences_copy, dev_tensors = prepare_corpus(dev_corpus, chars2i, words2i, device)
    gold_dev_constituents = [T.get_constituents(tree, filter_root=True) for tree in dev_corpus]

    sample_train_corpus = train_corpus[:len(dev_sentences)//4]
    corpus_tmp = prepare_corpus(sample_train_corpus, chars2i, words2i, device)
    sample_train_sentences, sample_train_raw_sentences, sample_train_sentences_copy, sample_train_tensors = corpus_tmp

    for t in sample_train_corpus:
        t.expand_unaries()
    sample_gold_train_constituents = [T.get_constituents(tree, filter_root=True) for tree in sample_train_corpus]
    for t in sample_train_corpus:
        t.merge_unaries()

    gold_train_constituents = [T.get_constituents(tree, filter_root=False) for tree in train_corpus]

    features = [extract_features(device, labels2i, sentence) for sentence in train_sentences]

    dynamic_features = [None for _ in features]

    logger.info("Deep copying train tensors...")
    train_tensors_copy = copy.deepcopy(train_tensors)

    tag_features = [extract_tags(device, tags2i, sentence) for sentence in train_sentences]

    idxs = list(range(len(train_sentences)))
    idxst = list(range(len(train_sentences)))
    num_tokens = sum([len(sentence) for sentence in train_sentences])

    outlogger = open("{}/learning_log".format(args.model), "w", buffering=1)
    outlogger.write("Epoch pl   tl     tp   tr   tf   tup  tur  tuf  tt   dp   dr   df   dup  dur  duf  dt discoF disco2F\n")

    logger.info("Starting training")
    best_dev_f = 0
    for epoch in range(1, args.i+1):
        epoch_loss = 0
        tag_eloss = 0

        if args.v > 0:
            logger.info("Stochastic replacement started")
        batch_stochastic_replacement(device,
                                    train_tensors,
                                    train_tensors_copy,
                                    words2i,
                                    pword=0.3)
        if args.v > 0:
            logger.info("Stochastic replacement done")

        model.train()
        grad_norm_p = 0
        grad_norm_t = 0

        if args.B == 1:
            for i, exs in enumerate(zip(idxs, idxst)):
                ex, ext = exs
                if i % 100 == 0 and args.v > 0:
                    logger.info("Epoch {} Training sent {} / {}".format(epoch, i, len(idxs)))

                tag_eloss += train_sentence_tagging(device, model, optimizer, train_tensors_copy[ext], tag_features[ext])
                if args.G is not None:
                    grad_norm_t += torch.nn.utils.clip_grad_norm_(model.parameters(), args.G)
                optimizer.step()

                epoch_loss += train_sentence_batch(device, model, optimizer,
                                                   train_tensors_copy[ex],
                                                   features[ex],
                                                   dynamic=dynamic_features[ex])
                if args.G is not None:
                    grad_norm_p += torch.nn.utils.clip_grad_norm_(model.parameters(), args.G)
                optimizer.step()

            grad_norm_p /= len(idxs)
            grad_norm_t /= len(idxs)
        else:
            # TODO: no dynamic oracle for batch mode
            n_batches = len(idxs)//args.B
            for i in range(n_batches):

                if i % 100 == 0 and args.v > 0:
                    logger.info("Epoch {} Training batch sent {} / {}".format(epoch, i, n_batches))

                start = i * args.B
                end = start + args.B

                input_tens = [train_tensors_copy[idx] for idx in idxs[start:end]]
                tag_tens = [tag_features[idx] for idx in idxs[start:end]]
                features_tens = [features[idx] for idx in idxs[start:end]]

                tag_tens, input_tens, features_tens = zip(*sorted(zip(tag_tens, input_tens, features_tens), 
                                                                   key = lambda x: len(x[0]), reverse=True))

                tag_eloss += train_sentence_tagging(device, model, optimizer, input_tens, tag_tens, batch=True) * args.B
                if args.G is not None:
                    grad_norm_t += torch.nn.utils.clip_grad_norm_(model.parameters(), args.G)
                optimizer.step()

                epoch_loss += train_sentence_batch(device, model, optimizer, input_tens, features_tens, batch=True) * args.B
                if args.G is not None:
                    grad_norm_p += torch.nn.utils.clip_grad_norm_(model.parameters(), args.G)
                optimizer.step()

            grad_norm_p /= n_batches
            grad_norm_t /= n_batches

        random.shuffle(idxs)
        random.shuffle(idxst)


        if args.dyno is not None:
            oracle_examples = extract_dyn_oracle_from_corpus(
                                           device, model,
                                           labels2i, i2labels,
                                           train_sentences_copy,
                                           train_tensors,
                                           gold_train_constituents, args.dyno)
            dynamic_features = [None for _ in features]
            for i, all_feats in oracle_examples:
                dynamic_features[i] = all_feats


        epoch_loss /= len(train_sentences)
        tag_eloss /= num_tokens
        if epoch % args.E != 0:
            summary = "Ep{} lr={:.5f} Tr l={:.5f} tl={:.5f} normp={:.3f} normt={:.3f}"
            print(summary.format(epoch, optimizer.param_groups[0]['lr'], 
                                 epoch_loss, tag_eloss,
                                 grad_norm_p, grad_norm_t), flush=True)
            continue

        optimizer.zero_grad()
        model.eval()

        if args.O == "asgd":
            optimizer.average()

        dpred_trees = predict_corpus(device, model, i2labels, i2tags, dev_sentences_copy, dev_tensors, batch=True)
        dcorpus_pred_constituents = [T.get_constituents(tree, filter_root=True) for tree in dpred_trees]
        p, r, f, up, ur, uf = Fscore_corpus(gold_dev_constituents, dcorpus_pred_constituents)
        dtag = eval_tagging(dev_sentences, dev_sentences_copy)

        outfile = "{}/tmp_dev.discbracket".format(args.model)
        with open(outfile, "w") as fstream:
            for tree in dpred_trees:
                fstream.write("{}\n".format(str(tree)))

        discop, discor, discodop_f = discodop_eval.call_eval(args.dev.replace(".ctbk", ".discbracket"), outfile)
        disco2p, disco2r, discodop2_f = discodop_eval.call_eval(args.dev.replace(".ctbk", ".discbracket"),
                                                                outfile, disconly=True)

        tpred_trees = predict_corpus(device, model,
                                     i2labels, i2tags,
                                     sample_train_sentences_copy,
                                     sample_train_tensors, batch=True)

        tcorpus_pred_constituents = [T.get_constituents(tree, filter_root=True) for tree in tpred_trees]
        tp, tr, tf, tup, tur, tuf = Fscore_corpus(sample_gold_train_constituents, tcorpus_pred_constituents)
        ttag = eval_tagging(sample_train_sentences, sample_train_sentences_copy)

        summary = "Ep{} lr={:.5f} Tr l={:.5f} tl={:.5f} pr{}/{} f={:.2f} u={}/{}/{:.1f} t={:.2f} Dev pr{}/{} f={:.2f} ({:.2f}) u={}/{}/{:.1f} t={:.2f}"
        print(summary.format(epoch, optimizer.param_groups[0]['lr'],
                             epoch_loss, tag_eloss, 
                             int(tp), int(tr), tf,
                             int(tup), int(tur), tuf, ttag, 
                             int(p), int(r), f, discodop_f,
                             int(up), int(ur), uf, dtag), flush=True)

        summary = "{} {:.4f} {:.4f} " + " ".join(["{:.1f}" for i in range(16)]) + "\n"
        outlogger.write(summary.format(epoch, epoch_loss, tag_eloss,
                                       tp, tr, tf, tup, tur, tuf, ttag,
                                       p, r, f, up, ur, uf, dtag, discodop_f, discodop2_f))

        if discodop_f > best_dev_f:
            best_dev_f = discodop_f
            model.cpu()
            torch.save(model, "{}/model".format(args.model))
            with open(f"{args.model}/best_dev_score", "w") as ff:
                ff.write(str(best_dev_f))
            if args.bert in {"replace", "concat"}:
                torch.save(model.bert, "{}/model_fine_tuned_bert".format(args.model))
            model.to(device)

        if args.O == "asgd":
            optimizer.cancel_average()

    if args.O == "asgd":
        optimizer.average()

def read_raw_corpus(filename):
    sentences = []
    with open(filename) as f:
        for line in f:
            # For negra
            line = line.replace("(", "#LRB#").replace(")", "#RRB#")
            line = line.strip().split()
            if len(line) > 0:
                sentences.append(line)
    return sentences

def main_eval(args, logger, device):
    
    model = torch.load("{}/model".format(args.model), map_location=device)
    model.to(device)
    model.eval()
    logger.info("Model loaded")
    
    i2labels, labels2i = load_dict("{}/i2labels".format(args.model))
    i2tags, tags2i = load_dict("{}/i2tags".format(args.model))
    i2chars, chars2i = load_dict("{}/i2chars".format(args.model))
    i2words, words2i = load_dict("{}/i2words".format(args.model))

    sentences = read_raw_corpus(args.corpus)
    sentence_toks = [[T.Token(token, i, [None]) for i, token in enumerate(sentence)] for sentence in sentences]


    with torch.no_grad():
        sent_tensors = [sentence_to_tensors(sent, chars2i, words2i, device) for sent in sentences]

        start = time.time()
        trees = predict_corpus(device, model, i2labels, i2tags, sentence_toks, sent_tensors, batch=True)
        end = time.time()
        n_sentences = len(sentences)
        n_tokens = sum([len(sent) for sent in sentences])
        p_time = end - start
        logger.info("parsing time: {:.2f} seconds for {} sentences ({} tokens)".format(p_time, n_sentences, n_tokens))
        logger.info("parsing time: {:.2f} sentences per second, {:.2f} tokens per second".format(n_sentences / p_time, n_tokens / p_time))


        if args.output is None:
            for tree in trees:
                print(tree)
        else:
            with open(args.output, "w") as f:
                for tree in trees:
                    f.write("{}\n".format(str(tree)))

            if args.gold is not None:
                p, r, f = discodop_eval.call_eval(args.gold, args.output, disconly=False)
                dp, dr, df = discodop_eval.call_eval(args.gold, args.output, disconly=True)

                print("precision={}".format(p))
                print("recall={}".format(r))
                print("fscore={}".format(f))
                print("disc-precision={}".format(dp))
                print("disc-recall={}".format(dr))
                print("disc-fscore={}".format(df))

#    counts = defaultdict(int)
#    for m in model.memory_sizes:
#        counts[m] += 1
#    print()
#    for k, v in sorted(counts.items()):
#        print("{}\t{}".format(k, v))
#    print()

def main(args, logger, device):
    """
        Discoparset: transition based discontinuous constituency parser

    warning: CPU trainer is non-deterministic (due to multithreading approximation)
    """
    if args.mode == "train":
        main_train(args, logger, device)
    else:
        main_eval(args, logger, device)

if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    import argparse

    usage = main.__doc__

    parser = argparse.ArgumentParser(description = usage, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    subparsers = parser.add_subparsers(dest="mode", description="Execution modes", help='train: training, eval: test')
    subparsers.required = True

    train_parser = subparsers.add_parser("train", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    eval_parser = subparsers.add_parser("eval", formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # train corpora
    train_parser.add_argument("model", help="Directory (to be created) for model exportation")
    train_parser.add_argument("train", help="Training corpus")
    train_parser.add_argument("dev",   help="Dev corpus")
    train_parser.add_argument("--fmt", help="Format for train and dev corpus", choices=["ctbk", "discbracket"], default="ctbk")

    # general options
    train_parser.add_argument("--gpu", type=int, default=None, help="Use GPU if available")
    train_parser.add_argument("-t", type=int, default=1, help="Number of threads for torch cpu")
    train_parser.add_argument("-S", type=int, default=None, help="Use only X first training examples")
    train_parser.add_argument("-v", type=int, default=1, choices=[0,1], help="Verbosity level")

    # training options
    train_parser.add_argument("-i", default=100, type=int, help="Number of epochs")
    train_parser.add_argument("-l", default=0.01, type=float, help="Learning rate")
    train_parser.add_argument("-m", default=0, type=float, help="Momentum (for sgd and asgd)")
    train_parser.add_argument("-d", default=1e-7, type=float, help="Decay constant for learning rate")
    train_parser.add_argument("-E", default=10, type=int, help="Evaluate on dev every E epoch")

    train_parser.add_argument("-A", action="store_true", help="Use attention-based context for structure classifier")
    train_parser.add_argument("-I", type=float, default=0.1, help="Embedding initialization uniform on [-I, I]")
    train_parser.add_argument("-G", default=100, type=float, help="Max norm for gradient clipping")

    train_parser.add_argument("-B", type=int, default=1, help="Size of batch")
    train_parser.add_argument("-O", default="asgd", choices=["adam", "sgd", "asgd"], help="Optimizer")
    train_parser.add_argument("-s", type=int, default=10, help="Random seed")

    train_parser.add_argument("-K", default=0.2, type=float, help="Dropout for character embedding layer")
    train_parser.add_argument("-Q", default=0, type=float, help="Dropout for char bi-LSTM output")
    train_parser.add_argument("-D", default=0.5, type=float, help="Dropout for parser output layers")
    train_parser.add_argument("-X", default=0.5, type=float, help="Dropout for tagger")

    train_parser.add_argument("-H", type=int, default=200, help="Dimension of hidden layer for FF nets")
    train_parser.add_argument("-C", type=int, default=100, help="Dimension of char bilstm")
    train_parser.add_argument("-c", type=int, default=100, help="Dimension of char embeddings")

    train_parser.add_argument("-w", type=int, default=None, help="Use word embeddings with dim=w")
    train_parser.add_argument("-W", type=int, default=400, help="Dimension of sentence bi-LSTM")

    train_parser.add_argument("-P", type=int, default=2, help="Depth of word transducer, min=2")
    

    train_parser.add_argument("--dyno", type=float, default=None, help="Use the dynamic oracle")

    # bert:
    train_parser.add_argument("--bert", choices=["replace", "concat"],
                              default=None, help="Use Bert as lexical model. replace: bert only. add: bert + char-lstm")
    train_parser.add_argument("--bert-id",
                              default=None, type=str,
                              help="Which BERT model to use? e.g. bert-base-cased")

    # test corpus
    eval_parser.add_argument("model", help="Pytorch model")
    eval_parser.add_argument("corpus", help="Test corpus, 1 tokenized sentence per line")
    eval_parser.add_argument("output", help="Outputfile")
    eval_parser.add_argument("--gold", default=None, help="Gold corpus (discbracket). If provided, eval with discodop")

    # test options
    eval_parser.add_argument("--gpu", type=int, default=None, help="Use GPU <int> if available")
    eval_parser.add_argument("-v", type=int, default=1, choices=[0,1], help="Verbosity level")
    eval_parser.add_argument("-t", type=int, default=1, help="Number of threads for torch cpu")


    args = parser.parse_args()

    for k, v in vars(args).items():
        print(k, v)
    
    torch.set_num_threads(args.t)

    logger = logging.getLogger()
    logger.info("Mode={}".format(args.mode))

    if args.mode == "train":
        os.makedirs(args.model, exist_ok = True)

    if args.gpu is not None:
        os.environ["CUDA_VISIBLE_DEVICES"]=str(args.gpu)

    use_cuda = torch.cuda.is_available()
    if use_cuda and args.gpu is not None:
        logger.info("Using gpu {}".format(args.gpu))
        device = torch.device("cuda".format(args.gpu))
    else:
        logger.info("Using cpu")
        device = torch.device("cpu")
    
    SEED = 0
    if args.mode == "train":
        SEED = args.s
    random.seed(SEED)
    np.random.seed(SEED)
    torch.manual_seed(SEED)

    main(args, logger, device)




