# Note: has word embeddings, gradient clipping and gaussian noise

if [ "$#" -le 2 ]
then
    echo "illegal number of parameters"
    echo Usage:
    echo    "sh expe_master.sh (negra|dptb|tiger_spmrl) modelname *args"
    exit 1
fi

source /home/getalp/coavouxm/anaconda3/bin/activate discoparset
mod=$2
mkdir -p ${mod}

git rev-parse HEAD > ${mod}/git_rev
conda list --explicit > ${mod}/conda-requirements.txt
pip freeze > ${mod}/requirements.txt

corpus=$1

tr=~/data/multilingual_disco_data/data/${corpus}/train.ctbk
dev=~/data/multilingual_disco_data/data/${corpus}/dev.ctbk

dtok=~/data/multilingual_disco_data/data/${corpus}/dev.tokens
ttok=~/data/multilingual_disco_data/data/${corpus}/test.tokens

dgold=~/data/multilingual_disco_data/data/${corpus}/dev.discbracket
tgold=~/data/multilingual_disco_data/data/${corpus}/test.discbracket


shift
shift


args="${tr} ${dev} $@"

python ../src/main.py train ${mod} ${args}                                                          > ${mod}/log.txt 2> ${mod}/err.txt &&
python ../src/main.py eval ${mod} ${dtok} ${mod}/dev_pred.discbracket  --gpu 0 -t 2 --gold ${dgold} > ${mod}/eval_dev &&
python ../src/main.py eval ${mod} ${ttok} ${mod}/test_pred.discbracket --gpu 0 -t 2 --gold ${tgold} > ${mod}/eval_test

