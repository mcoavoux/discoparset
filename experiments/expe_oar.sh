#  --gpu GPU             Use GPU if available (default: None)
#  -t T                  Number of threads for torch cpu (default: 1)
#  -S S                  Use only X first training examples (default: None)
#  -v {0,1}              Verbosity level (default: 1)
#  -i I                  Number of epochs (default: 100)
#  -l L                  Learning rate (default: 0.01)
#  -m M                  Momentum (for sgd and asgd) (default: 0)
#  -d D                  Decay constant for learning rate (default: 1e-07)
#  -E E                  Evaluate on dev every E epoch (default: 10)
#  -A                    Use attention-based context for structure classifier
#                        (default: False)
#  -I I                  Embedding initialization uniform on [-I, I] (default:
#                        0.1)
#  -G G                  Max norm for gradient clipping (default: 100)
#  -B B                  Size of batch (default: 1)
#  -O {adam,sgd,asgd}    Optimizer (default: asgd)
#  -s S                  Random seed (default: 10)
#  -K K                  Dropout for character embedding layer (default: 0.2)
#  -Q Q                  Dropout for char bi-LSTM output (default: 0)
#  -D D                  Dropout for parser output layers (default: 0.5)
#  -X X                  Dropout for tagger (default: 0.5)
#  -H H                  Dimension of hidden layer for FF nets (default: 200)
#  -C C                  Dimension of char bilstm (default: 100)
#  -c C                  Dimension of char embeddings (default: 100)
#  -w W                  Use word embeddings with dim=w (default: None)
#  -W W                  Dimension of sentence bi-LSTM (default: 400)
#  -P P                  Depth of word transducer, min=2 (default: 2)
#  --dyno DYNO           Use the dynamic oracle (default: None)
#  --bert {replace,concat}
#                        Use Bert as lexical model. replace: bert only. add:
#                        bert + char-lstm (default: None)
#  --bert-id BERT_ID     Which BERT model to use? e.g. bert-base-cased
#                        (default: None)


dataset=dptb

threads=1
iterations=60
lr=0.01
ep=2

K=0
Q=0

c=100
C=100
W=400
H=200
B=1



modelname=14jan/dptb_replace
mkdir -p ${modelname}

hyper="-t ${threads} -i ${iterations}  -E ${ep} -c ${c} -C ${C} -W ${W} -H ${H}    -B ${B}  -w 32 -Q ${Q} -K ${K} -l ${lr} -s 10  -O asgd -G 100 -I 0.1   --gpu 0 -t 1 --bert replace --bert-id bert-base-cased"
oarsub -l /core=8/gpu=1,walltime=72 "bash expe_master.sh ${dataset} ${modelname} ${hyper}"

modelname=14jan/dptb_concat
mkdir -p ${modelname}

hyper="-t ${threads} -i ${iterations}  -E ${ep} -c ${c} -C ${C} -W ${W} -H ${H}    -B ${B}  -w 32 -Q ${Q} -K ${K} -l ${lr} -s 10  -O asgd -G 100 -I 0.1   --gpu 0 -t 1 --bert concat --bert-id bert-base-cased"
oarsub -l /core=8/gpu=1,walltime=72 "bash expe_master.sh ${dataset} ${modelname} ${hyper}"
